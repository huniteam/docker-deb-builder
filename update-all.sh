#!/bin/bash

set -e

for i in jessie precise squeeze trusty wheezy; do
    ./docker.sh build $i
    ./docker.sh push $i
done
