#!/bin/bash

TAG_NAME=${2:-trusty}

case $TAG_NAME in

    squeeze)
        BASE=debian:squeeze
        ;;

    jessie)
        BASE=debian:jessie
        ;;

    precise)
        BASE=ubuntu:precise
        ;;

    trusty)
        BASE=ubuntu:trusty
        ;;

    wheezy)
        BASE=debian:wheezy
        ;;

    *)
        echo unknown base $TAG_NAME
        echo choose one of: precise, trusty, squeeze, wheezy, jessie
        exit 1;
        ;;
esac

IMAGE_NAME=huniteam/deb-builder:$TAG_NAME

case $1 in

    build)
        sed -e "s/%BASE%/$BASE/" Dockerfile.in | docker build -t $IMAGE_NAME -
        ;;

    push)
        docker push $IMAGE_NAME
        ;;

    *)
        echo "usage: $0 [build|push]"
        exit 1
        ;;

esac

